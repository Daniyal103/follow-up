import { Component, ComponentFactoryResolver, HostListener, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.scss']
})
export class PublicComponent implements OnInit {

  @ViewChild("home", { read: ViewContainerRef }) home: any;
  @ViewChild("service", { read: ViewContainerRef }) service: any;
  @ViewChild("projects", { read: ViewContainerRef }) projects: any;
  @ViewChild("about", { read: ViewContainerRef }) about: any;
  @ViewChild("contact", { read: ViewContainerRef }) contact: any;

  constructor(private readonly componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    this.loadHomeComponent();
    this.loadServiceComponent();
    this.loadProjectsComponent();
    this.loadAboutComponent();
    this.loadContactComponent();
  }

  loadHomeComponent() {
    import('./home/home.component').then( 
      ({ HomeComponent }) => {
        const component = this.componentFactoryResolver.resolveComponentFactory(
          HomeComponent
        );
        const componentRef = this.home.createComponent(
          component
        );
      }
    );
  }

  loadServiceComponent() {
    import('./service/service.component').then(
      ({ ServiceComponent }) => {
        const component = this.componentFactoryResolver.resolveComponentFactory(
          ServiceComponent
        );
        const componentRef = this.service.createComponent(
          component
        );
      }
    );
  }

  loadProjectsComponent() {
    import('./projects/projects.component').then(
      ({ ProjectsComponent }) => {
        const component = this.componentFactoryResolver.resolveComponentFactory(
          ProjectsComponent
        );
        const componentRef = this.projects.createComponent(
          component
        );
      }
    );
  }

  loadAboutComponent() {
    import('./about/about.component').then(
      ({ AboutComponent }) => {
        const component = this.componentFactoryResolver.resolveComponentFactory(
          AboutComponent
        );
        const componentRef = this.about.createComponent(
          component
        );
      }
    );
  }

  loadContactComponent() {
    import('./contact/contact.component').then(
      ({ ContactComponent }) => {
        const component = this.componentFactoryResolver.resolveComponentFactory(
          ContactComponent
        );
        const componentRef = this.contact.createComponent(
          component
        );
      }
    );
  }

}
