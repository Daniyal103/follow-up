import { Location } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private location: Location) {
    window.scrollTo({
      top: 1,
      left: 0
    })
  }

  scrollPosition: any = 0;
  onload = false;
  url: any = "#home";
  isMenuShow = true;

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    this.scrollPosition = document.documentElement.scrollTop;
    if (!this.onload) {
      this.scrollPosition = this.scrollPosition + 55;
      this.onload = true;
      window.scrollTo({
        top: this.scrollPosition,
        left: 0
      })
    }
    this.url = window.location.href.split("/")[3];
  }

  ngOnInit(): void {
    this.showNavigationBar();
    setTimeout(() => {
      if (document.documentElement.scrollTop == 0) {
        window.scrollTo({
          top: 0.5,
          left: 0
        })
        setTimeout(() => {
          window.scrollTo({
            top: 0,
            left: 0
          })
        }, 100)
      }
    }, 100)
    var self = this;
    $(window).scroll(function (event: any) {
      clearTimeout($.data(event?.target, 'scrollTimer'));
      $.data(event.target, 'scrollTimer', setTimeout(function () {
        if ($('#home').position().top <= self.scrollPosition && $('#service').position().top - 1 > self.scrollPosition) {
          self.location.replaceState("/#home");
          self.url = '#home'
        } else if ($('#service').position().top <= self.scrollPosition && $('#projects').position().top - 1 > self.scrollPosition) {
          self.location.replaceState("/#service");
          self.url = '#service'
        } else if ($('#projects').position().top <= self.scrollPosition && $('#about').position().top - 1 > self.scrollPosition) {
          self.location.replaceState("/#projects");
          self.url = '#projects'
        } else if ($('#about').position().top <= self.scrollPosition && $('#contact').position().top - 1 > self.scrollPosition) {
          self.location.replaceState("/#about");
          self.url = '#about'
        } else if ($('#contact').position().top <= self.scrollPosition) {
          self.location.replaceState("/#contact");
          self.url = '#contact'
        }
      }, 50));
    });
  }

  showNavigationBar(){
    if(!this.isMenuShow){
      document.getElementById("mobile-menu")!.style.maxHeight = "100%";
      this.isMenuShow = true;
    } else {
      document.getElementById("mobile-menu")!.style.maxHeight = "0px";
      document.getElementById("mobile-menu")!.style.overflow = "hidden";
      this.isMenuShow = false;
    }
  }

}
